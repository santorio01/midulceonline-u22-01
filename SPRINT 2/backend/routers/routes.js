const { Router } = require('express');
var controllerInfoUser = require('../controllers/controllerInfoUser');
var controllerProduct = require('../controllers/controllerProduct');
var controllerOrder = require('../controllers/controllerOrder');
const router = Router();

router.get('/find_user/:id', controllerInfoUser.findUserById);
router.get('/list_users/:idb?', controllerInfoUser.listInfoUsers);
router.post('/user', controllerInfoUser.saveInfoUser);
router.delete('/user/:id', controllerInfoUser.deleteInfoUser);
router.put('/user/:id', controllerInfoUser.updateInfoUser);

router.get('/find_product/:id', controllerProduct.findProductById);
router.get('/list_products/:idb?', controllerProduct.listProducts);
router.post('/product', controllerProduct.saveProduct);
router.delete('/product/:id', controllerProduct.deleteProduct);
router.put('/product/:id', controllerProduct.updateProduct);

router.get('/find_order/:id', controllerOrder.findOrderById);
router.get('/list_orders/:idb?', controllerOrder.listOrders);
router.post('/order', controllerOrder.saveOrder);
router.delete('/order/:id', controllerOrder.deleteOrder);
router.put('/order/:id', controllerOrder.updateOrder);

module.exports = router;