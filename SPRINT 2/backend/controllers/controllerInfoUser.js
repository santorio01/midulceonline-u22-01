const InfoUser = require("../models/InfoUser");

function saveInfoUser(req,res){
    var infoUser = new InfoUser(req.body);
    infoUser.save((err,result) =>{
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
        res.status(200).send({message:result});}
    });
}

function findUserById(req,res){
    var idInfoUser = req.params.id;
    InfoUser.findById(idInfoUser).exec((err,result)=>{
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro no se encuentra'});
            }else{
                res.status(200).send({result});
            }
        }
    });
};

function listInfoUsers(req,res){
    var idInfoUser = req.params.idb;
    if(!idInfoUser){
        var result = InfoUser.find({}).sort('email');
    }else{
        var result = InfoUser.find({InfoUser:idInfoUser}).sort('email');
    }

    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro no se encuentra'});
            }else{
                res.status(200).send({result});
            }
        }
    });
};

function updateInfoUser(req,res){
    var idInfoUser = req.params.id;
    InfoUser.findOneAndUpdate({_id: idInfoUser}, req.body, {new: true}, function(err,InfoUser){
        if(err){
            return res.json(500, {
                message: 'No se ha encontrado el elemento'
            })
        }
        return res.status(200).json(InfoUser);
    });
};

function deleteInfoUser(req,res){
    var idInfoUser = req.params.id;
    InfoUser.findByIdAndRemove(idInfoUser, function(err, InfoUser){
        if(err){
            return res.json(500, {
                message: 'No se ha encontrado el elemento'
            })
        }
        return res.status(200).json(InfoUser);
    });
};

module.exports={
    listInfoUsers,
    findUserById,
    saveInfoUser,
    updateInfoUser,
    deleteInfoUser
};