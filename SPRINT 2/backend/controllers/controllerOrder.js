const Order = require("../models/Order");

function saveOrder(req,res){
    var myOrder = new Order(req.body);
    myOrder.save((err,result) =>{
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
        res.status(200).send({message:result});}
    });
}

function findOrderById(req,res){
    var idOrder = req.params.id;
    Order.findById(idOrder).exec((err,result)=>{
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro no se encuentra'});
            }else{
                res.status(200).send({result});
            }
        }
    });
};

function listOrders(req,res){
    var idOrder = req.params.idb;
    if(!idOrder){
        var result = Order.find({}).sort('email');
    }else{
        var result = Order.find({Order:idOrder}).sort('email');
    }

    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro no se encuentra'});
            }else{
                res.status(200).send({result});
            }
        }
    });
};

function updateOrder(req,res){
    var idOrder = req.params.id;
    Order.findOneAndUpdate({_id: idOrder}, req.body, {new: true}, function(err,Order){
        if(err){
            return res.json(500, {
                message: 'No se ha encontrado el elemento'
            })
        }
        return res.status(200).json(Order);
    });
};

function deleteOrder(req,res){
    var idOrder = req.params.id;
    Order.findByIdAndRemove(idOrder, function(err, Order){
        if(err){
            return res.json(500, {
                message: 'No se ha encontrado el elemento'
            })
        }
        return res.status(200).json(Order);
    });
};

module.exports={
    findOrderById,
    listOrders,
    saveOrder,
    updateOrder,
    deleteOrder
};