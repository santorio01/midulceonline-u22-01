const Product = require("../models/Product");

function saveProduct(req,res){
    var myProduct = new Product(req.body);
    myProduct.save((err,result) =>{
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
        res.status(200).send({message:result});}
    });
}

function findProductById(req,res){
    var idProduct = req.params.id;
    Product.findById(idProduct).exec((err,result)=>{
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro no se encuentra'});
            }else{
                res.status(200).send({result});
            }
        }
    });
};

function listProducts(req,res){
    var idProduct = req.params.idb;
    if(!idProduct){
        var result = Product.find({}).sort('title');
    }else{
        var result = Product.find({Product:idProduct}).sort('title');
    }

    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error en la ejecución de la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro no se encuentra'});
            }else{
                res.status(200).send({result});
            }
        }
    });
};

function updateProduct(req,res){
    var idProduct = req.params.id;
    Product.findOneAndUpdate({_id: idProduct}, req.body, {new: true}, function(err,Product){
        if(err){
            return res.json(500, {
                message: 'No se ha encontrado el elemento'
            })
        }
        res.status(200).json(Product);
    });
};

function deleteProduct(req,res){
    var idProduct = req.params.id;
    Product.findByIdAndRemove(idProduct, function(err, Product){
        if(err){
            return res.json(500, {
                message: 'No se ha encontrado el elemento'
            })
        }
        res.status(200).json(Product);
    });
};

module.exports={
    findProductById,
    listProducts,
    saveProduct,
    updateProduct,
    deleteProduct
};