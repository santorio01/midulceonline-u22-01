var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = Schema({
    title:{
        type : String,
        required : true,
    },
    price:{
        type : String,
        required : true,
    },
    image:{
        type : String,
        required : true,
    },
    description:{
        type : String,
        required : true,
    }
});

const Product = mongoose.model('product', ProductSchema);
module.exports = Product;