var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = Schema({
    email:{
        type : String,
        required : true,
    },
    date_purchase:{
        type : String,
        required : true,
    },
    pay_method:{
        type : String,
        required : true,
    },
    products: [
        {
            _id : false,
            id : String,
            quantity : Number
        }
    ]
});

const Order = mongoose.model('order', OrderSchema);
module.exports = Order;