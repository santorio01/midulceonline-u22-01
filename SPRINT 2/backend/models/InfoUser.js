var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InfoUserSchema = Schema({
    email:{
        type : String,
        required : true,
    },
    name:{
        type : String,
        required : true,
    },
    phone:{
        type : String,
        required : true,
    },
    address:{
        type : String,
        required : true,
    },
    city:{
        type : String,
        required : true,
    }
});

const InfoUser = mongoose.model('infoUser', InfoUserSchema);
module.exports = InfoUser;