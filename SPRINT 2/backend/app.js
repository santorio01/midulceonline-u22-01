var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cors = require('cors');

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({
    extended: true
}));

app.use(require('./routers/routes.js'));

module.exports = app;