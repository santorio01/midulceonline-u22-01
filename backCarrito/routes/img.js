const express = require('express')
const router = express.Router()

const path = require('path');

//Img Path
const imgFolderPath = path.join(__dirname, '../img/')

//Images
router.get('/:imgName', (req, res) =>{
    const image = req.params.imgName;
    res.sendFile(`${imgFolderPath}${image}`)
});

module.exports = router;