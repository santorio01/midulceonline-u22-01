const express = require('express');
const getItemById = require('../data/getItemById');
const router = express.Router();

//MODELS
const Item = require('../models/Item');


//GET ITEMS
router.get('/', async (req, res) =>{
    try{
        const itemFromDB = await Item.find();
        res.json(itemFromDB);
    }catch (err) {
        res.json({ messege: err.messege });
    }
});

//GET ITEM
router.get('/item', async (req, res) =>{
    const itemId = req.body.itemId;
    try{
        const itemFromDB = await getItemById(itemId);
        res.json(itemFromDB);
    }catch (err) {
        res.json({ messege: err.messege });
    }
});

//CREATE ITEM
router.post('/',async (req, res) =>{
    console.log(req.body);
    const item = new Item({
        title : req.body.title,
        price : req.body.price,
        image : req.body.image
    });
    try{
        const newItem = await item.save();
        res.json(newItem);
    }catch (err) {
        res.json({ messege: err.messege });
    }
});

module.exports = router;